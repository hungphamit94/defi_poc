pragma solidity ^0.5.0;

import "./DappToken.sol";
import "./USDTToken.sol";

contract TokenFarm {
    string public name = "Dapp Token Farm";
    address public owner;
    DappToken public dappToken;
    USDTToken public usdtToken;

    address[] public stakers;
    mapping(address => uint) public stakingBalance;
    mapping(address => Stake[]) stakes;
    mapping(address => bool) public hasStaked;
    mapping(address => bool) public isStaking;

    struct Stake {
        uint8 numberDay;
        uint amount;
        uint dateStart;
    }

    constructor(DappToken _dappToken, USDTToken _usdtToken) public {
        dappToken = _dappToken;
        usdtToken = _usdtToken;
        owner = msg.sender;
    }

    function stakeTokens(uint _amount, uint8 _numberDay) public {
        require(_amount > 0, "amount cannot be 0");

        Stake memory s = Stake(_numberDay, _amount, block.timestamp);

        stakes[msg.sender].push(s);

        usdtToken.transferFrom(msg.sender, address(this), _amount);
        stakingBalance[msg.sender] = stakingBalance[msg.sender] + _amount;

        if(!hasStaked[msg.sender]) {
            stakers.push(msg.sender);
        }

        isStaking[msg.sender] = true;
        hasStaked[msg.sender] = true;
    }

    // Unstaking Tokens (Withdraw)
    function unstakeTokens(uint8 _id) public {
        uint balance = stakes[msg.sender][_id].amount;

        require(balance > 0, "staking balance cannot be 0");

        require((stakes[msg.sender][_id].dateStart + stakes[msg.sender][_id].numberDay * 1 days ) < (block.timestamp), "staking still yet withdraw");
        
        usdtToken.transfer(msg.sender, balance);

        for(uint8 i=_id;i<stakes[msg.sender].length-1; i++) {
            stakes[msg.sender][i]= stakes[msg.sender][i+1];
        }
        stakes[msg.sender].pop();

        if (stakes[msg.sender].length == 0) {
            stakingBalance[msg.sender] = 0;
            isStaking[msg.sender] = false;
        }
    }

    function getLengthStake() public view returns(uint) {
        return stakes[msg.sender].length;
    }

    function getListStake(uint8 n) public view returns (uint8[] memory, uint[] memory, uint[] memory){
        require(n==stakes[msg.sender].length, 'Number Stake had changed');
        uint8[] memory numberDays = new uint8[](n);
        uint[] memory amounts = new uint[](n);
        uint[] memory dateStarts = new uint[](n);
        for(uint i=0; i < stakes[msg.sender].length; i++) {
            numberDays[i] = stakes[msg.sender][i].numberDay;
            amounts[i] = stakes[msg.sender][i].amount;
            dateStarts[i] = stakes[msg.sender][i].dateStart;
        }
        return (numberDays, amounts, dateStarts);
    }

    // Issuing Tokens
    function issueTokens() public {
        // Only owner can call this function
        require(msg.sender == owner, "caller must be the owner");

        // Issue tokens to all stakers
        for (uint i=0; i<stakers.length; i++) {
            address recipient = stakers[i];
            for (uint j=0; j < stakes[recipient].length; j++) {
                if ((stakes[recipient][j].dateStart+stakes[recipient][j].numberDay * 1 days) >= block.timestamp){
                    if(stakes[recipient][j].amount > 0) {
                        dappToken.transfer(recipient, stakes[recipient][j].amount);
                    }
                }
            }
        }
    }
}
