import React, { Component } from 'react'
import Web3 from 'web3'
import USDTToken from '../abis/USDTToken.json'
import DappToken from '../abis/DappToken.json'
import TokenFarm from '../abis/TokenFarm.json'
import Navbar from './Navbar'
import Main from './Main'
import './App.css'

class App extends Component {

  async componentWillMount() {
    await this.loadWeb3()
    await this.loadBlockchainData()
    await this.getLengthStake()
  }

  async loadBlockchainData() {
    const web3 = window.web3

    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })

    const networkId = await web3.eth.net.getId()

    // Load USDTToken
    const usdtTokenData = USDTToken.networks[networkId]
    if(usdtTokenData) {
      const usdtToken = new web3.eth.Contract(USDTToken.abi, usdtTokenData.address)
      this.setState({ usdtToken })
      let usdtTokenBalance = await usdtToken.methods.balanceOf(this.state.account).call()
      this.setState({ usdtTokenBalance: usdtTokenBalance.toString() })
    } else {
      window.alert('USDTToken contract not deployed to detected network.')
    }

    // Load DappToken
    const dappTokenData = DappToken.networks[networkId]
    if(dappTokenData) {
      const dappToken = new web3.eth.Contract(DappToken.abi, dappTokenData.address)
      this.setState({ dappToken })
      let dappTokenBalance = await dappToken.methods.balanceOf(this.state.account).call()
      this.setState({ dappTokenBalance: dappTokenBalance.toString() })
    } else {
      window.alert('DappToken contract not deployed to detected network.')
    }


    // Load TokenFarm
    const tokenFarmData = TokenFarm.networks[networkId]
    if(tokenFarmData) {
      const tokenFarm = new web3.eth.Contract(TokenFarm.abi, tokenFarmData.address)
      this.setState({ tokenFarm })
      let stakingBalance = await tokenFarm.methods.stakingBalance(this.state.account).call()
      this.setState({ stakingBalance: stakingBalance.toString() })
    } else {
      window.alert('TokenFarm contract not deployed to detected network.')
    }

    this.setState({ loading: false })

  }

  async loadWeb3() {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
      await window.ethereum.enable()
    }
    else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
    }
    else {
      window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
    }
  }

  stakeTokens = async (amount, day) => {
    this.setState({ loading: true })
    //var n = 0;
    this.state.usdtToken.methods.approve(this.state.tokenFarm._address, amount).send({ from: this.state.account }).on('transactionHash', (hash) => {
      this.state.tokenFarm.methods.stakeTokens(amount, day).send({ from: this.state.account }).on('transactionHash',(hash) => {
        this.setState({ loading: false })
      }).then(async function () {
        let n = 0;
        await this.state.tokenFarm.methods.getLengthStake().call({ from: this.state.account }, function(err, res){
            n = res;
        })
        await this.stakes(n);
        await this.setState({usdtTokenBalance: ((window.web3.utils.fromWei(this.state.usdtTokenBalance, "Ether") - window.web3.utils.fromWei(amount, "Ether"))).toString()+"000000000000000000"})
      }.bind(this))
      
    })
  }

  getLengthStake = () => {
    this.state.tokenFarm.methods.getLengthStake().call({ from: this.state.account }, function(err, res){
      this.stakes(res)
    }.bind(this))
  }

  unstakeTokens = (id) => {
    this.setState({ loading: true })
    this.state.tokenFarm.methods.unstakeTokens(id).send({ from: this.state.account }).on('transactionHash', (hash) => {
      // console.log(hash)
      // this.getLengthStake()
      document.location.reload(true);
      // this.setState({ loading: false })
    })
    
  }

  stakes = (number) => {
    this.state.tokenFarm.methods.getListStake(number).call({ from: this.state.account }, function(err, res){
      this.setState({numbersDay: res[0]});
      this.setState({amounts: res[1]});
      this.setState({startDates: res[2]});
    }.bind(this))
  }

  constructor(props) {
    super(props)
    this.state = {
      account: '0x0',
      usdtToken: {},
      dappToken: {},
      numbersDay: [],
      amounts: [],
      startDates: [],
      tokenFarm: {},
      usdtTokenBalance: '0',
      dappTokenBalance: '0',
      stakingBalance: '0',
      loading: true
    }
  }

  render() {
    let content
    if(this.state.loading) {
      content = <p id="loader" className="text-center">Loading...</p>
    } else {
      content = <Main
        usdtTokenBalance={this.state.usdtTokenBalance}
        dappTokenBalance={this.state.dappTokenBalance}
        stakingBalance={this.state.stakingBalance}
        stakeTokens={this.stakeTokens}
        amounts={this.state.amounts}
        numbersDay={this.state.numbersDay}
        startDates={this.state.startDates}
        unstakeTokens={this.unstakeTokens}
      />
    }

    return (
      <div>
        <Navbar account={this.state.account} />
        <div className="container-fluid mt-5">
          <div className="row">
            <main role="main" className="col-lg-12 ml-auto mr-auto" style={{ maxWidth: '800px' }}>
              <div className="content mr-auto ml-auto">
                <a
                  href="#"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                </a>

                {content}

              </div>
            </main>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
