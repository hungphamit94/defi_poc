import React, { Component } from 'react'
import usdt from '../usdt.png'

class Main extends Component {
  constructor(props) {
      super(props);
      this.state = {
          day: 0,
      }
      this.daysChoice = [7,10,20,30];
      this.handleChoiceDay = this.handleChoiceDay.bind(this);
  }

  handleChoiceDay(day) {
    this.setState({day: day})
  }

  render() {
    var showTagDaysChoice = [];
    this.daysChoice.forEach(element => {
        if (element == this.state.day) {
          showTagDaysChoice.push(<a style={{padding: 10, borderRadius: 20, border: "solid 1px black", float: 'left', marginRight: 20, background: 'black', color: "white"}} onClick={()=>this.handleChoiceDay(element)}>{element} Days</a>)
        }else{
          showTagDaysChoice.push(<a style={{padding: 10, borderRadius: 20, border: "solid 1px black", float: 'left', marginRight: 20}} onClick={()=>this.handleChoiceDay(element)}>{element} Days</a>)
        }
    })
    var length = this.props.amounts.length;
    var rows = [];
    this.props.amounts.forEach((value, index) => {
      var milliseconds = this.props.startDates[index] * 1000 
      var dateObject = new Date(milliseconds)
      var row = <tr scope="row">
          <td>{window.web3.utils.fromWei(this.props.amounts[index], 'Ether')}</td>
          <td>{dateObject.toLocaleString()}</td>
          <td>{this.props.numbersDay[index]}</td>
          <td>{window.web3.utils.fromWei(this.props.dappTokenBalance, 'Ether')}</td>
          <td>
            <button
              type="submit"
              className="btn btn-link btn-block btn-sm"
              onClick={(event) => {
                event.preventDefault()
                this.props.unstakeTokens(index)
              }}>
                UN-STAKE...
              </button>
          </td>
      </tr>
      rows.push(row)
    })
    return (
      <div id="content" className="mt-2">

        <table className="table table-striped text-muted text-center">
          <thead>
            <tr>
              <th scope="col">Staking Balance (USDT)</th>
              <th scope="col">Day Stake</th>
              <th scope="col">Period</th>
              <th scope="col">Reward Balance (USDT)</th>
              <th scope="col">Unstake</th>
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </table>

        <div className="card mb-4" >

          <div className="card-body">

            <form className="mb-3" onSubmit={(event) => {
                event.preventDefault()
                let amount
                amount = this.input.value.toString()
                amount = window.web3.utils.toWei(amount, 'Ether')
                this.props.stakeTokens(amount, this.state.day)
              }}>
              <div>
                <label className="float-left"><b>Stake Tokens</b></label>
                <span className="float-right text-muted">
                  Balance: {window.web3.utils.fromWei(this.props.usdtTokenBalance, 'Ether')}
                </span>
              </div>
              <div className="input-group mb-4">
                <input
                  type="text"
                  ref={(input) => { this.input = input }}
                  className="form-control form-control-lg"
                  placeholder="0"
                  required />
                <div className="input-group-append">
                  <div className="input-group-text">
                    <img src={usdt} height='32' alt=""/>
                    &nbsp;&nbsp;&nbsp; USDT
                  </div>
                </div>
              </div>

              <div>
                <label className="float-left"><b>Day Stake</b></label>
              </div>
              <div className="input-group mb-4">
                {showTagDaysChoice}
              </div>

              <button type="submit" className="btn btn-primary btn-block btn-lg">STAKE!</button>
            </form>
            
          </div>
        </div>

      </div>
    );
  }
}

export default Main;
