const DappToken = artifacts.require('DappToken')
const USDTToken = artifacts.require('USDTToken')
const TokenFarm = artifacts.require('TokenFarm')

module.exports = async function(deployer, network, accounts) {
  // Deploy Mock USDT Token
  await deployer.deploy(USDTToken)
  const usdtToken = await USDTToken.deployed()

  // Deploy Dapp Token
  await deployer.deploy(DappToken)
  const dappToken = await DappToken.deployed()

  // Deploy TokenFarm
  await deployer.deploy(TokenFarm, dappToken.address, usdtToken.address)
  const tokenFarm = await TokenFarm.deployed()

  // Transfer all tokens to TokenFarm (1 million)
  await dappToken.transfer(tokenFarm.address, '1000000000000000000000000')

  // Transfer 100 Mock USDT tokens to investor
  await usdtToken.transfer(accounts[1], '100000000000000000000')
}
